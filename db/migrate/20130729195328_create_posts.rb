class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.integer :user_id
      t.integer :group_id
      t.string :media_content
      t.string :text_content

      t.timestamps
    end
  end
end
