namespace :db do
  desc "Fill database with sample data"
  task populate: :environment do
    make_users
    make_groups
    make_memberships
    make_posts
  end
end

def make_users
  admin = User.create!(username:     "matt",
                       display_name: "Matt Ferrell",
                       email:    "mattferrell2@gmail.com",
                       password: "gEAHMr17",
                       password_confirmation: "gEAHMr17")
                       #admin: true)
  99.times do |n|
    first_name = Faker::Name.first_name
    last_name = Faker::Name.last_name
    name = first_name + " " + last_name
    username = first_name.downcase[0,1] + last_name.downcase
    email = "example-#{n+1}@railstutorial.org"
    password  = "password"
    User.create!(username:     username,
                 display_name: name,
                 email:    email,
                 password: password,
                 password_confirmation: password)
  end
end

def make_groups
  10.times do |n|
    name = Faker::Company.name
    description = Faker::Company.catch_phrase
    Group.create!(name: name,
                  description: description,
                  visibile: true)
  end
end

def make_posts
  users = User.all(limit: 6)
  10.times do
    content = Faker::Lorem.sentence(5)
    users.each { |user| user.posts.create!(text_content: content, group_id: user.groups.find(1).id ) }
  end
end

def make_memberships
  users = User.all
  groups = Group.all
  group = groups.first

  users.each{ |user| user.follow!(group)}

  #user  = users.first
  #followed_users = users[2..50]
  #followers      = users[3..40]
  #followed_users.each { |followed| user.follow!(followed) }
  #followers.each      { |follower| follower.follow!(user) }
end
