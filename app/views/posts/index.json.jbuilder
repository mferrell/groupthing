json.array!(@posts) do |post|
  json.extract! post, :user_id, :group_id, :media_content, :text_content
  json.url post_url(post, format: :json)
end
