class User < ActiveRecord::Base
	has_many :posts, dependent: :destroy
	has_many :memberships, foreign_key: "user_id", dependent: :destroy
	has_many :groups, through: :memberships, source: :group

  has_secure_password
  before_save { self.email = email.downcase }
  before_create :create_remember_token
  validates :username, presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence:   true,
                    format:     { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, length: { minimum: 6 }


  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.encrypt(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def post_feed
    Post.where("user_id = ?", id)
  end

  def group_feed
    Membership.where("user_id = ?", id)
    #memberships.map{ |item| Group.find(item.group_id) }
    #Post.where("user_id = ?", id)
  end

   def following?(other_group)
    memberships.find_by(group_id: other_group.id)
  end

  def follow!(other_group)
    memberships.create!(group_id: other_group.id)
  end

  def unfollow!(other_group)
    memberships.find_by(group_id: other_group.id).destroy!
  end

  private

    def create_remember_token
      self.remember_token = User.encrypt(User.new_remember_token)
    end
end
