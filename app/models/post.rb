class Post < ActiveRecord::Base
	belongs_to :user
	belongs_to :group
	validates :text_content, length: { maximum: 140 }

end
