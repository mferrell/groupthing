class Group < ActiveRecord::Base
	has_many :posts

#	has_many :memberships
#	has_many :users, through: :memberships
	has_many :reverse_memberships, foreign_key: "group_id",
                                   class_name:  "Membership",
                                   dependent:   :destroy
    has_many :users, through: :reverse_memberships, source: :user

    def post_feed
    	Post.where("group_id = ?", id)
  	end

end
