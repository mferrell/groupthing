class StaticPagesController < ApplicationController
  def home
  	if signed_in?
  		  @post  = current_user.posts.build if signed_in?
        @group = Group.find(1)
      	@post_feed_items = current_user.post_feed.paginate(page: params[:page])
        @group_feed_items = current_user.group_feed.paginate(page: params[:page])
    end
  end

  def help
  end

  def about
  end

end
